# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='player',
            name='last_approach',
            field=models.DateTimeField(verbose_name='date published'),
            preserve_default=True,
        ),
    ]
