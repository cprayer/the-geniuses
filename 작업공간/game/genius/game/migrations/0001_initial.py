# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('nickname', models.CharField(unique=True, max_length=20)),
                ('card', models.PositiveIntegerField(default=0)),
                ('prev_card', models.PositiveIntegerField(default=0)),
                ('chip', models.PositiveIntegerField(default=0)),
                ('bet', models.PositiveIntegerField(default=0)),
                ('is_joined', models.BooleanField(default=False)),
                ('is_my_turn', models.BooleanField(default=False)),
                ('is_show_result', models.PositiveIntegerField(default=0)),
                ('last_approach', models.DateTimeField(auto_now_add=True, auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Room',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(unique=True, max_length=40)),
                ('is_playing', models.BooleanField(default=False)),
                ('current_player_number', models.PositiveIntegerField(default=0)),
                ('stack', models.PositiveIntegerField(default=0)),
                ('join_players', models.ManyToManyField(null=True, to='game.Player')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
