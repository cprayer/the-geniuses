from django.contrib import admin
from game.models import Player, Room

# Register your models here.

admin.site.register(Player)
admin.site.register(Room)
